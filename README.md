## mido-user 7.0 NRD90M V9.6.1.0.NCFMIFD release-keys
- Manufacturer: xiaomi
- Platform: msm8953
- Codename: mido
- Brand: Xiaomi
- Flavor: lineage_mido-userdebug
- Release Version: 10
- Id: QQ3A.200805.001
- Incremental: eng.zeelog.20201101.082203
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: xiaomi/mido/mido:7.0/NRD90M/V9.6.1.0.NCFMIFD:user/release-keys
- OTA version: 
- Branch: mido-user-7.0-NRD90M-V9.6.1.0.NCFMIFD-release-keys
- Repo: xiaomi_mido_dump_8324


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
