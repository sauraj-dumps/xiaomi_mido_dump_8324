#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:22433792:08387ca0bfb255b3d2b1c0df2936daa8f5c858a7; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:13215744:4b99cdc0de11e6d5fbf8cda49d879134a70d6e73 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:22433792:08387ca0bfb255b3d2b1c0df2936daa8f5c858a7 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
